package mars.venus;

import mars.Globals;
import mars.Settings;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

public class SettingsUIAction extends GuiAction {
    Settings settings;

    SettingsUIAction(String name, Icon icon, String des,
                     Integer mnemonic, KeyStroke accel, VenusUI gui) {
        super(name, icon, des, mnemonic, accel, gui);
        settings = Globals.getSettings();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SettingUIDialog settingUIDialog = new SettingUIDialog(mainUI, "UI Settings");
        settingUIDialog.pack();
        settingUIDialog.setLocationRelativeTo(mainUI);
        settingUIDialog.setVisible(true);
    }

    private class SettingUIDialog extends JDialog {
        JPanel uiSettings, uiTableSettings, uiTableHeightSetting;

        SettingUIDialog(JFrame owner, String title) {
            super(owner, title);

            uiSettings = new UISetting();
            uiTableSettings = new UITableSetting();
            uiTableHeightSetting = new UITableHeightSetting();
            this.setLayout(new BorderLayout());
            this.add(uiSettings, BorderLayout.NORTH);
            this.add(uiTableSettings, BorderLayout.CENTER);
            this.add(uiTableHeightSetting, BorderLayout.SOUTH);
        }

        private class UISetting extends JPanel {
            UISetting() {
                JSlider fontSizeSlider;
                JSpinner fontSizeSpinner;
                JLabel fontSizeLabel = new JLabel("UI font size:");
                fontSizeSlider = buildUIFontSizeSlider();
                fontSizeSpinner = buildFontSizeSpinner();
                fontSizeSlider.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent changeEvent) {
                        JSlider spinner = (JSlider) changeEvent.getSource();
                        fontSizeSpinner.setValue((int) spinner.getValue());
                    }
                });

                fontSizeSpinner.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent changeEvent) {
                        JSpinner slider = (JSpinner) changeEvent.getSource();
                        fontSizeSlider.setValue((int) slider.getValue());
                    }
                });

                JButton applyButton = new JButton("Apply");
                applyButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        Font font = settings.getFontByPosition(Settings.UI_FONT);
                        int size = (int) fontSizeSpinner.getValue();
                        font = font.deriveFont(font.getStyle(), size);
                        settings.setFontByPosition(Settings.UI_FONT, font);
                    }
                });

                JButton defaultButton = new JButton("Default");

                defaultButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        Font font = settings.getDefaultFontByPosition(Settings.UI_FONT);
                        int size = font.getSize();
                        fontSizeSlider.setValue(size);
                        fontSizeSpinner.setValue(size);
                    }
                });

                JPanel ctrl = new JPanel();
                ctrl.setLayout(new FlowLayout(FlowLayout.LEFT));
                ctrl.add(fontSizeLabel);
                ctrl.add(fontSizeSlider);
                ctrl.add(fontSizeSpinner);
                ctrl.add(applyButton);
                ctrl.add(defaultButton);

                JLabel tips = new JLabel("You need to restart the program after press the apply button.");

                this.setLayout(new BorderLayout());
                this.add(ctrl, BorderLayout.CENTER);
                this.add(tips, BorderLayout.SOUTH);
                this.setBorder(BorderFactory.createTitledBorder("UI settings."));
            }

            private JSlider buildUIFontSizeSlider() {
                int valueMax = VenusUI.FONT_SIZE_MAX;
                int valueMin = VenusUI.FONT_SIZE_MIN;
                int value = settings.getFontByPosition(Settings.UI_FONT).getSize();
                JSlider res = new JSlider(valueMin, valueMax, value);
                res.setToolTipText("Use slider to select ui font size from " + valueMin + " to " + valueMin + ".");
                return res;
            }

            private JSpinner buildFontSizeSpinner() {
                int valueMax = VenusUI.FONT_SIZE_MAX;
                int valueMin = VenusUI.FONT_SIZE_MIN;
                int value = settings.getFontByPosition(Settings.UI_FONT).getSize();
                JSpinner res = new JSpinner(new SpinnerNumberModel(value, valueMin, valueMax, 1));
                res.setToolTipText("Set ui font size.");
                return res;
            }
        }

        private class UITableSetting extends JPanel {
            int[] fontPositionArray = {Settings.EVEN_ROW_FONT,
                    Settings.ODD_ROW_FONT, Settings.TEXTSEGMENT_HIGHLIGHT_FONT,
                    Settings.TEXTSEGMENT_DELAYSLOT_HIGHLIGHT_FONT,
                    Settings.DATASEGMENT_HIGHLIGHT_FONT,
                    Settings.REGISTER_HIGHLIGHT_FONT,
                    Settings.TABLE_NORMAL_FONT
            };

            UITableSetting() {
                this.setLayout(new GridLayout(fontPositionArray.length, 1));

                for (int fontPosition : fontPositionArray) {
                    JPanel ctrl = new JPanel();
                    ctrl.setLayout(new FlowLayout(FlowLayout.LEFT));

                    JSlider fontSizeSlider;
                    JSpinner fontSizeSpinner;
                    String label = settings.getFontSizeKeyByPosition(fontPosition);
                    JLabel fontSizeLabel = new JLabel(label);

                    fontSizeSlider = buildUIFontSizeSlider(fontPosition);
                    fontSizeSpinner = buildFontSizeSpinner(fontPosition);
                    fontSizeSlider.addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent changeEvent) {
                            JSlider spinner = (JSlider) changeEvent.getSource();
                            fontSizeSpinner.setValue((int) spinner.getValue());
                        }
                    });

                    fontSizeSpinner.addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent changeEvent) {
                            JSpinner slider = (JSpinner) changeEvent.getSource();
                            fontSizeSlider.setValue((int) slider.getValue());
                        }
                    });

                    JButton applyButton = new JButton("Apply");
                    applyButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            Font font = settings.getFontByPosition(fontPosition);
                            int size = (int) fontSizeSpinner.getValue();
                            font = font.deriveFont(font.getStyle(), size);
                            settings.setFontByPosition(fontPosition, font);
                            mainUI.pack();
                        }
                    });

                    JButton defaultButton = new JButton("Default");

                    defaultButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            Font font = settings.getDefaultFontByPosition(fontPosition);
                            int size = font.getSize();
                            fontSizeSlider.setValue(size);
                            fontSizeSpinner.setValue(size);
                        }
                    });

                    ctrl.add(fontSizeLabel);
                    ctrl.add(fontSizeSlider);
                    ctrl.add(fontSizeSpinner);
                    ctrl.add(applyButton);
                    ctrl.add(defaultButton);
                    this.add(ctrl);
                }
                this.setBorder(BorderFactory.createTitledBorder("Table settings."));
            }

            private JSlider buildUIFontSizeSlider(int pos) {
                int valueMax = VenusUI.FONT_SIZE_MAX;
                int valueMin = VenusUI.FONT_SIZE_MIN;
                int value = settings.getFontByPosition(pos).getSize();
                JSlider res = new JSlider(valueMin, valueMax, value);
                res.setToolTipText("Use slider to select ui " + settings.getFontSizeKeyByPosition(pos) + " from " + valueMin + " to " + valueMin + ".");
                return res;
            }

            private JSpinner buildFontSizeSpinner(int pos) {
                int valueMax = VenusUI.FONT_SIZE_MAX;
                int valueMin = VenusUI.FONT_SIZE_MIN;
                int value = settings.getFontByPosition(pos).getSize();
                JSpinner res = new JSpinner(new SpinnerNumberModel(value, valueMin, valueMax, 1));
                res.setToolTipText("Set ui " + settings.getFontSizeKeyByPosition(pos) + ".");
                return res;
            }
        }

        private class UITableHeightSetting extends JPanel {
            int[] tableHeightPositionArray = {Settings.REGISTER_TABLE_HEIGHT,
                    Settings.COP0_TABLE_HEIGHT,
                    Settings.COP1_TABLE_HEIGHT,
                    Settings.TEXT_SEGMENT_TABLE_HEIGHT,
                    Settings.DATA_SEGMENT_TABLE_HEIGHT
            };

            UITableHeightSetting() {
                this.setLayout(new GridLayout(tableHeightPositionArray.length + 1, 1));

                for (int pos : tableHeightPositionArray) {
                    JPanel ctrl = new JPanel();
                    ctrl.setLayout(new FlowLayout(FlowLayout.LEFT));

                    JSlider heightSizeSlider;
                    JSpinner heightSizeSpinner;
                    String label = settings.getSizeKeyByPosition(pos);
                    JLabel fontSizeLabel = new JLabel(label);

                    heightSizeSlider = buildUIHeightSizeSlider(pos);
                    heightSizeSpinner = buildUIHeightSizeSpinner(pos);
                    heightSizeSlider.addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent changeEvent) {
                            JSlider spinner = (JSlider) changeEvent.getSource();
                            heightSizeSpinner.setValue((int) spinner.getValue());
                        }
                    });

                    heightSizeSpinner.addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent changeEvent) {
                            JSpinner slider = (JSpinner) changeEvent.getSource();
                            heightSizeSlider.setValue((int) slider.getValue());
                        }
                    });

                    JButton applyButton = new JButton("Apply");
                    applyButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            int size = (int) heightSizeSpinner.getValue();
                            settings.setSizeByPosition(pos, size);
                            mainUI.pack();
                        }
                    });

                    JButton defaultButton = new JButton("Default");

                    defaultButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent actionEvent) {
                            int size = settings.getDefaultSizeByPosition(pos);
                            heightSizeSlider.setValue(size);
                            heightSizeSpinner.setValue(size);
                        }
                    });

                    ctrl.add(fontSizeLabel);
                    ctrl.add(heightSizeSlider);
                    ctrl.add(heightSizeSpinner);
                    ctrl.add(applyButton);
                    ctrl.add(defaultButton);
                    this.add(ctrl);
                }
                JLabel tips = new JLabel("You need to restart the program after press the apply button.");
                this.add(tips);
                this.setBorder(BorderFactory.createTitledBorder("Height settings."));
            }

            private JSlider buildUIHeightSizeSlider(int pos) {
                int valueMax = VenusUI.TABLE_HEIGHT_SIZE_MAX;
                int valueMin = VenusUI.TABLE_HEIGHT_SIZE_MIN;
                int value = settings.getSizeByPosition(pos);
                JSlider res = new JSlider(valueMin, valueMax, value);
                res.setToolTipText("Use slider to select " + settings.getSizeKeyByPosition(pos) + " from " + valueMin + " to " + valueMin + ".");
                return res;
            }

            private JSpinner buildUIHeightSizeSpinner(int pos) {
                int valueMax = VenusUI.TABLE_HEIGHT_SIZE_MAX;
                int valueMin = VenusUI.TABLE_HEIGHT_SIZE_MIN;
                int value = settings.getSizeByPosition(pos);
                JSpinner res = new JSpinner(new SpinnerNumberModel(value, valueMin, valueMax, 1));
                res.setToolTipText("Set " + settings.getSizeKeyByPosition(pos) + ".");
                return res;
            }
        }
    }
}
